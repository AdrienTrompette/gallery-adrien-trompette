<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class PaintingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();
        $categories = $manager->getRepository(Category::class)->findAll();

        for($i = 1; $i <= 40; $i++) {
            $painting = new Painting();
            $painting->setTitle($faker->words($faker->numberBetween(1, 2), true))
                     ->setDescription($faker->paragraphs(3, true))
                     ->setCreatedAt($faker->dateTimeBetween('-30 days', 'now'))
                     ->setHeight($faker->randomFloat(2, 50, 120))
                     ->setWidth($faker->randomFloat(2, 50, 120))
                     ->setImage($i.'.jpg')
                     ->setCategory($categories[$faker->numberBetween(0, count($categories) -1)]);
            $manager->persist($painting);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}


