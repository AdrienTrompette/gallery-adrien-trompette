<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Form\PaintingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{

    #[Route('/', name: 'home')]
    /**
     * Display home page
     * @return Response
     */
    public function viewHome(): Response
    {
        return $this->render('pages/home.html.twig');
    }


    #[Route('/about', name: 'about')]
    /**
     * Display about page
     * @return Response
     */
    public function viewAbout(): Response
    {
        return $this->render('pages/about.html.twig');
    }


    #[Route('/team', name: 'team')]
    /**
     * Display team page
     * @return Response
     */
    public function viewTeam(): Response
    {
        return $this->render('pages/team.html.twig');
    }
}
