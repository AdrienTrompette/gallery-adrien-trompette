<?php


namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Painting;
use App\Form\CommentType;
use App\Repository\CategoryRepository;
use App\Repository\PaintingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController {

    /**
     * @param PaintingRepository $repository
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @return Response
     */
    #[Route('/gallery', name: 'gallery')]
    public function viewGallery(PaintingRepository $repository, CategoryRepository $categoryRepository,  Request $request): Response
    {
        $paintings = $repository->findAll();
        $categories = $categoryRepository->findBy(
                [],
                ['name' => 'ASC']
            );

        $byCategory = $request->query->get('category');

        return $this->render('pages/gallery.html.twig', [
            'paintings' => $paintings,
            'categories' => $categories,
            'categoryId' => $byCategory,
        ]);
    }


    #[Route('/gallery-detail/{id}', name:'gallery-detail')]
    /**
     * Display detail of each painting
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    public function detailPainting(Painting $painting, EntityManagerInterface $manager, Request $request): Response
    {
        $comment = new Comment;
        $comment_form = $this->createForm(CommentType::class, $comment);
        $comment_form->handleRequest($request);
        if($comment_form->isSubmitted() && $comment_form->isValid()) {
            $comment->setPainting($painting);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute('gallery-detail', ['id' => $painting->getId()]);
        }
        return $this->render('pages/detail.html.twig', [
            'painting' => $painting,
            'commentForm' => $comment_form->createView(),
            'comment' => $comment
        ]);
    }
}

