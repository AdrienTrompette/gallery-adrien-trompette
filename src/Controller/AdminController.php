<?php

namespace App\Controller;

use App\Entity\Painting;
use App\Form\PaintingType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController {

    #[Route('/admin', name: 'admin')]
    /**
     * Display admin page
     * @return Response
     */
    public function viewAdmin(): Response
    {
        $paintings = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->findAll();
        return $this->render('pages/admin.html.twig', [
            'paintings' => $paintings
        ]);
    }


    #[Route('/addpainting', name: 'addpainting')]
    /**
     * Add a painting
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function addPainting(Request $request, EntityManagerInterface $manager): Response
    {
        $painting = new Painting;
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $painting->setCreatedAt(new \DateTimeImmutable());
            $painting->setImage('default.png');
            $manager->persist($painting);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('pages/add.html.twig', [
            'form'     => $form
        ]);
    }


    #[Route('/delpainting/{id}', name: 'delpainting')]
    /**
     * Delete a painting
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delPainting(Painting $painting, EntityManagerInterface $manager): Response
    {
        $manager->remove($painting);
        $manager->flush();
        return $this->redirectToRoute('admin');
    }


    #[Route('/editpainting/{id}', name: 'editpainting')]
    /**
     * Edit a painting
     * @param Painting $painting
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    public function editPainting(Painting $painting, EntityManagerInterface $manager, Request $request): Response
    {
        $form = $this->createForm(PaintingType::class, $painting);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($painting);
            $manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render('pages/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
